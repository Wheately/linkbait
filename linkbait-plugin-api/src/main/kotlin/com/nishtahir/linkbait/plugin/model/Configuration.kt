package com.nishtahir.linkbait.plugin.model

import java.io.File

/**
 * Created by nish on 7/19/16.
 */
interface Configuration {

    fun getStaticFileDirectory(): File

    fun getPluginDirectory(): File

    fun getTemporaryFileDirectory(): File

    fun getPluginRepository(): File
}